<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/category/{slug?}', 'ShopController@category')->name('category');
Route::get('/product/{slug?}', 'ShopController@products')->name('product');

Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => ['auth'] ], function () {
    Route::group(['prefix'=> '/', 'middleware' => 'role:admin'],function (){
        Route::get('/', 'DashboardController@dashboard')->name('admin.index');
        Route::resource('/category', 'CategoryController', ['as'=>'admin']);
        Route::resource('/product', 'ProductController', ['as'=>'admin']);
        Route::resource('/price', 'PriceController', ['as'=>'admin']);
        Route::group(['prefix'=> 'user_management', 'namespace' => 'UserManagement'], function () {
            Route::resource('/users', 'UserController', ['as' => 'admin.user_management']);
        });

    });

});


Route::get('/', function () {
    return view('front.home');
});

Auth::routes();


Route::get('/public/uploads/{filename}', function ($filename)
{
    $path = public_path('/uploads/' . $filename);


    $file = File::get($path);
    $type = File::mimeType($path);

    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);

    return $response;
});



Route::get('/home', 'HomeController@index')->name('home');
