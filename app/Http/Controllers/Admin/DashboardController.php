<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\Category;
class DashboardController extends Controller
{
    public function dashboard() {
        return view('admin.dashboard', [
            'categories' => Category::lastCategories(5),
            'products' =>   Product::lastProduct(5),
            'count_categories' => Category::count(),
            'count_products' => Product::count()
        ]);
    }
}
