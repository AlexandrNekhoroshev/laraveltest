<?php

namespace App\Http\Controllers;

use App\Models\Price;
use App\Models\Product;
use App\Models\Category;
use Illuminate\Http\Request;

class ShopController extends Controller
{
    public function category($slug)
    {
        $category = Category::where('slug', $slug)->first();


        ($category == '') ? $category : $this->products($slug);


        return view('front.category', [
            'category' => $category,
            'products' => $category->products()->where('published', 1)->paginate(12)
        ]);
    }

    public function products($slug)
    {


        return view('front.product', [
            'product' => Product::where('slug', $slug)->first()
        ]);
    }
}
