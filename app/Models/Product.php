<?php

namespace App\Models;

use Carbon\Carbon;
use Faker\Provider\Image;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Str;

class Product extends Model
{
    protected $fillable = ['title', 'slug', 'price', 'description_short', 'description', 'meta_title', 'meta_description', 'meta_keywords', 'published', 'viewed', 'created_by', 'modified_by'];
    protected $base_dir = 'public/uploads';

    public function setSlugAttribute($value)
    {
        $this->attributes['slug'] = Str::slug(mb_substr($this->title, 0, 40) . '-' . Carbon::now()->format('dmyHi'), '-');
    }


    public function categories()
    {
        return $this->morphToMany('App\Models\Category', 'product_to_categories');
    }

    public function prices()
    {
        return $this->morphedByMany('App\Models\Price', 'price_to_products');
    }

    public function scopeLastProduct($query, $count)
    {
        return $query->orderBy('created_at', 'desc')->take($count)->get();
    }

    public function getPrice()
    {
        if ($this->prices()->count() > 0) {
            $date_prices = $this->prices()->pluck('date_price_end', 'date_price_start');
            $min_date = $this->getMinDatePrice($date_prices);
            $price = $this->getDatePrice($min_date);
            $this->price = $price;
            return $this->price;
        } else {
            return $this->price;
        }
    }

    private function getMinDatePrice($date_prices)
    {
        $date_array = array();
        foreach ($date_prices as $key => $date_price) {
            $date_array[$date_price] = (strtotime($date_price) - strtotime($key)) / 86400;
        }

        return array_keys($date_array, min($date_array));
    }

    private function getDatePrice($min_date)
    {
        $prices = $this->prices()->where('date_price_end', $min_date)->pluck('price', 'id');
        if (count($prices) > 1) {
            $prices = $this->getSortingPrice($prices);
            $prices = $this->prices()->where('id', $prices)->pluck('price')->implode('');
        } else {
            $prices = $prices->implode('');
        }
        return $prices;
    }

    private function getSortingPrice($prices)
    {
        $key_array = array();

        foreach ($prices as $key => $price) {
            $key_array[ ] = $key;
        }
        return max($key_array);

    }

}
