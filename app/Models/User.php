<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function roles()
    {
        return $this->belongsToMany('App\Models\Roles','user_roles','user_id','role_id');
    }

    /**
     *
     * @return boolean
     */
    public function isEmployee(){
        $roles = $this->roles->toArray();
        return !empty($roles);
    }

    /**
     * @return boolean
     */
    public function hasRole($check){
        return in_array($check, array_pluck($this->roles->toArray(), 'name'));
    }

    /**
     * @return int
     */
    private function getIdInArray($array, $term)
    {
        foreach ($array as $key => $value){
            if($value == $term){
                return $key+1;
            }
        }
        return false;
    }

    /**
     * @return boolean
     */
    public function makeEmployee($title)
    {
        $assinged_roles = array();
        $roles = array_feth(Roles::all()->toArray(), 'name');
        switch ($title){
            case 'admin' :
                $assinged_roles[ ] = $this->getIdInArray($roles, 'admin');
            case 'client' :
                $assinged_roles[ ] = $this->getIdInArray($roles, 'client');
                break;
            default:
                $assinged_roles[ ] = false;

        }
        $this->roles()->attach($assinged_roles);
    }
}
