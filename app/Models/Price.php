<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Price extends Model
{
    protected $fillable = ['title','product','date_price_start','date_price_end','price','created_at','updated_at'];

    public function products()
    {
        return $this->morphToMany('App\Models\Product', 'price_to_products');
    }
}
