@extends('layouts.app')
@section('title', $product->meta_title)
@section('meta_keywords', $product->meta_keywords)
@section('meta_description', $product->meta_description)
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <kbd >{{$product->title}}</kbd>
                <p>{!! $product->description !!}</p>
            </div>
        </div>
    </div>
@endsection