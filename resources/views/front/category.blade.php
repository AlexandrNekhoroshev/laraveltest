@extends('layouts.app')

@section('title', $category->title)

@section('content')
    <div class="container">

            @forelse($products as $product)
            <div class="row">
                <div class="col-sm-12">
                    <h2><a href="{{route('product', $product->slug)}}">
                            <kbd>
                                {{$product->title}}
                            </kbd>

                        </a></h2>
                    <blockquote class="blockquote">

                        <p class="mb-0"><p>{{$product->getPrice()  }}</p></p>
                    </blockquote>

                    <p>{!! $product->description_short !!}</p>
                </div>
            </div>
            @empty
                <h2 class="text-right">Пусто</h2>

                @endforelse
        {{$products->links()}}
    </div>
@endsection