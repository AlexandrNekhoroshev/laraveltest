@extends('admin.layouts.app_admin')

@section('content')

    <div class="container">
        @component('admin.components.breadcrumb')
            @slot('title') Редоктировать цену @endslot
            @slot('parent') Главная @endslot
            @slot('active') Цены @endslot
        @endcomponent
        <hr>
        <form class="form-horizontal" action="{{route('admin.price.update', $prices)}}" method="post">
            <input hidden name="_method" value="put">
            {{ csrf_field() }}

            {{-- Form include --}}
            @include('admin.price.partials.form')
        </form>
    </div>
@endsection