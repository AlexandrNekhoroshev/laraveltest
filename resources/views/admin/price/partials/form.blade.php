<label for="">Наименование</label>
<input type="text" class="form-control" name="title" placeholder="Заголовок категории"
       value="{{$prices->title or ''}}" required>
<label for="">Стоимость</label>
<select name="product" multiple class="form-control"  >
    <option value="0">Без товара</option>
    @foreach($products as $product)
        <option value="{{$product->id}}"
                @if(isset($prices->product))
                        {{$product->id == $prices->product}}
                @if($product->id == $prices->product)
                selected
                @endif
                @endif
        >{{$product->title}}</option>
    @endforeach
</select>
<label for="">Дата начала изменения цены</label>
<input type="date" class="form-control" name="date_price_start" placeholder="Дата начала изменения цены"
       value="{{$prices->date_price_start or ''}}" required>

<label for="">Дата окончания изменения цены</label>
<input type="date" class="form-control" name="date_price_end" placeholder="Дата окончания изменения цены"
       value="{{$prices->date_price_end or ''}}" required>
<label for="">Стоимость</label>
<input type="text" class="form-control" name="price" placeholder="Стоимость"
       value="{{$prices->price or ''}}" required>

<hr>
<input type="submit" class="btn btn-primary" value="Сохранить">