@extends('admin.layouts.app_admin')

@section('content')

    <div class="container">
        @component('admin.components.breadcrumb')
            @slot('title') Список цен @endslot
            @slot('parent') Главная @endslot
            @slot('active') Цены @endslot

        @endcomponent

        <hr>
        <a href="{{route('admin.price.create')}}" class="btn btn-primary float-right"><i
                    class="fa fa-plus-square-o"></i> Создать цену</a>
        <table class="table table-striped">
            <thead>
            <th>Наименование</th>
            <th>Публикация</th>
            <th class="text-right">Дата</th>
            </thead>
            <tbody>
            @forelse($prices as $price)
                <tr>
                    <td>{{$price->title}}</td>
                    <td>{{$price->date_price}}</td>
                    <td class="text-right">

                        <form onsubmit="if(confirm('Удалить')){ return true;}else{ return false}"
                              action="{{route('admin.price.destroy', $price)}}" method="post">
                            <input type="hidden" name="_method" value="DELETE">

                            <a href="{{route('admin.price.edit', $price)}}" class="btn btn-info">
                                <i class="fa fa-edit"></i>
                            </a>
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger"><i class="far fa-trash-alt"></i></button>
                        </form>
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="3" class="text-center">
                        <h2>Данные отсутствуют</h2>
                    </td>
                </tr>
            @endforelse
            </tbody>
            <tfoot>
            <tr>
                <td colspan="3">
                    <ul class="pagination pull-right">
                        {{ $prices->links() }}
                    </ul>
                </td>
            </tr>
            </tfoot>
        </table>
    </div>

@endsection