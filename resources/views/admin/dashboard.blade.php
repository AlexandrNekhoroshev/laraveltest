@extends('admin.layouts.app_admin')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <div class="jumbotron text-center display-3">
                    <p><span class="badge badge-primary ">Категорий {{$count_categories}}</span></p>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="jumbotron text-center display-3">
                    <p><span class="badge badge-primary ">Товаров {{$count_products}}</span></p>
                </div>
            </div>


        </div>

        <div class="col-sm-6 float-left">
                <a href="{{route('admin.category.create')}}" class="btn btn-block btn-default">Создать категорию</a>
                @foreach($categories as $category)
                    <div class="container list-group-item">
                        <div class="row">
                            <div class="col-sm-10 ">
                                <h4 class="list-group-item-heading">{{$category->title}}</h4>
                                <p class="list-group-item-text">{{$category->products()->count()}}</p>
                            </div>
                            <div class="col-sm-2 ">
                                <a class="btn-info btn"  href="{{route('admin.category.edit', $category)}}" >
                                    <i class="fa fa-edit"></i>
                                </a>
                            </div>
                        </div>
                    </div>

                    @endforeach()

        </div>
        <div class="col-sm-6 float-right">
                <a href="{{route('admin.product.create')}}" class="btn btn-block btn-default">Создать товар</a>
                    @foreach($products as $product)
                        <div class="container list-group-item">
                            <div class="row">
                                <div class="col-sm-10 ">
                                    <h4 class="list-group-item-heading">{{$product->title}}</h4>
                                    <p class="list-group-item-text">
                                        {{$product->categories()->pluck('title')->implode(', ')}}
                                    </p>
                                </div>
                                <div class="col-sm-2 ">
                                    <a class="btn-info btn"  href="{{route('admin.product.edit', $product)}}" >
                                        <i class="fa fa-edit"></i>
                                    </a>
                                </div>
                            </div>
                        </div>

                        @endforeach

            </div>
        </div>
    </div>

@endsection