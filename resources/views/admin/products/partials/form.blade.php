
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <label for="">Статус</label>
            <select name="published" class="form-control" >
                @if(isset($product->id))
                    <option value="0" @if($product->published == 0) selected="" @endif>Не опубликовано</option>
                    <option value="1" @if($product->published == 1) selected="" @endif>Опубликовано</option>
                @else
                    <option value="0">Не опубликовано</option>
                    <option value="1">Опубликовано</option>
                @endif
            </select>
            <label for="">Заголовок</label>
            <input type="text" class="form-control" name="title" placeholder="Заголовок категории"
                   value="{{$product->title or ''}}" required>
            <label for="">Ссылка</label>
            <input type="text" class="form-control" name="slug" placeholder="Генерируется автоматически"
                   value="{{$product->slug or ''}}" readonly>
            <label for="">Родительская категория</label>
            <select name="categories[]" multiple class="form-control" >
                <option value="0">Без родителя</option>
                @include('admin.products.partials.categories', ['categories' => $categories])
            </select>
            <label for="">Цена</label>
            <input type="text" class="form-control" name="price" placeholder="Цена на товар(Если не выставленна)"
                   value="{{$product->price or ''}}" >
        </div>
    </div>
</div>



<hr>

<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <label for="">Краткое описание новости</label>
            <textarea name="description_short" class="form-control" id="description_short">{{$product->description_short or ''}}</textarea>

            <label for="">Полное описание новости</label>
            <textarea name="description" class="form-control" id="description">{{$product->description or ''}}</textarea>
            <hr>

            <label for="">Мета заголовок</label>
            <input type="text" class="form-control" name="meta_title" placeholder="Мета заголовок"
                   value="{{$product->meta_title or ''}}" required>
            <label for="">Мета описание</label>
            <input type="text" class="form-control" name="meta_description" placeholder="Мета описание"
                   value="{{$product->meta_description or ''}}" required>
            <label for="">Ключевые слова</label>
            <input type="text" class="form-control" name="meta_keywords" placeholder="Ключевые слова через запятую"
                   value="{{$product->meta_keywords or ''}}" required>
        </div>
    </div>
</div>


<hr>
<input type="submit" class="btn btn-primary" value="Сохранить">